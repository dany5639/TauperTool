﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Windows.Forms;
using System.IO;

namespace ConsoleApp
{
    class GenericMemoryEditor
    {
        public static System.IntPtr hProc;

        public static uint TagAddress = 0x021271EC;
        public static Process edProcess;

        public static void Execute()
        {
            var processes = Process.GetProcessesByName("eldorado");

            if (processes.Length == 0)
            {
                Console.Error.WriteLine("Unable to find any eldorado.exe processes.");
            }

            edProcess = processes[0];

            hProc = OpenProcess(ProcessAccessFlags.All, false, (int)edProcess.Id);
        }

        public static void WriteMem(int address, byte[] value)
        {
            int outs;

            WriteProcessMemory(hProc, new IntPtr(address), value, (uint)value.Length, out outs);
        }

        #region Grunt Certified Memory Correction Tool
        const int PROCESS_VM_WRITE = 0x0020;
        const int PROCESS_VM_OPERATION = 0x0008;

        [DllImport("kernel32.dll")]
        static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, uint nSize, out int lpNumberOfBytesWritten);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress);

        [DllImport("kernel32.dll")]
        public static extern Int32 CloseHandle(IntPtr hProcess);

        [Flags]
        public enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            VMRead = 0x00000010,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000
        }
        #endregion

    }
}
