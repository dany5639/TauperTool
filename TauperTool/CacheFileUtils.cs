﻿using System;
using System.Windows.Forms;
using ConsoleApp;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Globalization;

namespace ConsoleApp
{
    class CacheFileUtils
    {
        public static bool ReplaceTags(List<string> args)
        {
            if (args.Count < 4)
                return false;

            var ODSTMapName = args[0];
            args.RemoveAt(0);

            var TagsListCsvPath = args[0];
            args.RemoveAt(0);

            var method = args[0];
            args.RemoveAt(0);

            if (!(method == "null" || method == "replace"))
                return false;

            // var pathStringHARDCODED = $"D:\\Halo\\Xenia\\ISO\\Halo3ODST\\maps\\{ODSTMapName}.map";
            // var ODSTtagnamesHARDCODED = @"D:\Halo\The Library\Tagnames\ODSTtags.csv";
            var pathString = ODSTMapName;
            var TagsListCsv = TagsListCsvPath;
            var inputSearchArrayString = "";
            var TagClassesToReplace = args;
            var nullTagID = "FFFFFFFF";
            var mapName = pathString.Split(".".ToCharArray()).First().Split("\\".ToCharArray()).Last();
            var validOdstMapFiles = new List<string> { "c100", "c200", "mainmenu", "h100", "l200", "l300", "sc100", "sc110", "sc120", "sc130", "sc140", "sc150" };
            var shaderTags = new List<string> { "rmsh", "rmtr", "rmhg", "rmfl", "rmcs", "rmss", "rmzo", "prt3", "decs", "rmzo", "rmct", "rmbk" };

            if (mapName == "c100" && TagClassesToReplace[0] == "rmsh")
                nullTagID = "E88C0716"; // E73805C2 = something, E7ED0677 = smg_metal; E88C0716 = bchai01

            var validReplacementTagnames = new List<string>
            {
                @"fx\particles\flare\light_spike", // prt3
                @"fx\scenery_fx\havok\havok_collection", // effe
                @"objects\characters\grunt\fx\grunt_birthday_party", // effe
                @"fx\decals\impact_bullet\impact_bullet_small\soft", // decs
                @"shaders\invalid" // rmsh
                // @"objects\vehicles\odst_pod\shaders\unsc_decal" // rmsh
            };

            // Check file
            var path = new FileInfo(pathString);

            if (!path.Exists)
                return false;

            var path2 = new FileInfo(TagsListCsv);

            if (!path2.Exists)
                return false;

            // Collect all valid tag indexes, to prevent writing garbage tag indexes
            var validTagsID = new List<string> { "FFFFFFFF" };

            var cachefile = pathString.Split("\\".ToCharArray()).Last().Split(".".ToCharArray()).First();
            using (var reader = new StreamReader(File.OpenRead(TagsListCsv)))
            {
                while (true)
                {
                    var line = reader.ReadLine();
                    if (line == null)
                        break;

                    var item = line.Split(",".ToCharArray());
                    // E1790004,matg,mainmenu,globals\globals

                    if (item[2] == cachefile)
                        validTagsID.Add(item[0]);

                    if (method == "replace")
                    {
                        if (item[1] == TagClassesToReplace[0] && item[2] == mapName && validReplacementTagnames.Contains(item[3]))
                        {
                            nullTagID = item[0];
                            Console.WriteLine($"Found potential replacement: {item[0]} {item[1]} {item[2]} {item[3]}");
                        }
                    }
                }
            }

            if (method == "replace" && nullTagID == "FFFFFFFF")
            {
                using (var reader = new StreamReader(File.OpenRead(TagsListCsv)))
                {
                    while (true)
                    {
                        var line = reader.ReadLine();
                        if (line == null)
                            break;

                        var item = line.Split(",".ToCharArray());
                        // E1790004,matg,mainmenu,globals\globals

                        if (item[2] == cachefile)
                            validTagsID.Add(item[0]);

                        if (method == "replace")
                        {
                            if (item[1] == TagClassesToReplace[0] && item[2] == mapName)
                            {
                                nullTagID = item[0];
                                Console.WriteLine($"Found potential replacement: {item[0]} {item[1]} {item[2]} {item[3]}");
                                break;
                            }
                        }
                    }
                }
            }

            // Scan file for patterns
            foreach (var patternTagClass in TagClassesToReplace)
            {
                var patternAddresses = new List<long>();

                using (var reader = new BinaryReader(File.OpenRead(pathString)))
                {
                    while (true)
                    {
                        inputSearchArrayString = "";
                        foreach (var a in patternTagClass.ToCharArray())
                            inputSearchArrayString = $"{inputSearchArrayString}{(byte)a:X2}";

                        if (patternTagClass.Length == 3)
                            inputSearchArrayString = $"{inputSearchArrayString}20";

                        // Convert search pattern to a list of bytes
                        var pattern = HexStringArrayToHex(inputSearchArrayString);
                        pattern.AddRange(new byte[8]);

                        // Console.WriteLine($"{reader.BaseStream.Position:X8}");

                        // Read a chunk of data before searching
                        var bufferSize = 0x1000;
                        var buffer = new byte[0];

                        if (reader.BaseStream.Position > reader.BaseStream.Length - bufferSize)
                            buffer = reader.ReadBytes((int)(reader.BaseStream.Length - reader.BaseStream.Position));
                        else
                            buffer = reader.ReadBytes(bufferSize);

                        if (reader.BaseStream.Position == reader.BaseStream.Length)
                            break;

                        // Make sure there isnt part of the pattern at the end of the buffer that wouldn't be caught. So go back the size of the pattern, before reading a new chunk for the buffer
                        reader.BaseStream.Position -= pattern.Count;

                        var bufferCounter = -1;
                        foreach (var bufferByte in buffer)
                        {
                            bufferCounter++;

                            if (bufferByte != pattern[0])
                                continue;

                            var foundCount = 0;
                            var patternCounter = -1;
                            foreach (var patternByte in pattern)
                            {
                                patternCounter++;

                                if (bufferCounter + patternCounter < buffer.Length)
                                {
                                    if (patternByte != buffer[bufferCounter + patternCounter])
                                        goto LabelNoMatch;
                                    else
                                        foundCount++;
                                }
                            }

                            if (foundCount == pattern.Count)
                            {
                                var defaultAddr = reader.BaseStream.Position;
                                var tempAddr = reader.BaseStream.Position - bufferSize + pattern.Count + bufferCounter;
                                Console.WriteLine($"{(tempAddr):X8} {patternTagClass} newTagID: {nullTagID}");

                                reader.BaseStream.Position = tempAddr + 0xC;

                                var tagID = BitConverter.GetBytes(reader.ReadUInt32()).ToArray();

                                if (!validTagsID.Contains($"{tagID[0]:X2}{tagID[1]:X2}{tagID[2]:X2}{tagID[3]:X2}"))
                                {
                                    ;
                                    // Console.WriteLine($"{reader.BaseStream.Position:X8} {tagID[0]:X2}{tagID[1]:X2}{tagID[2]:X2}{tagID[3]:X2}");
                                }
                                else
                                {
                                    patternAddresses.Add(tempAddr);
                                }

                                reader.BaseStream.Position = defaultAddr;

                            }

                            ;
                            // expected first address:       05856F68
                            // current first address result: 00000000

                            LabelNoMatch:
                            ;
                        }


                    }
                }
                // Now we should have a list of all addresses where the pattern was found
                using (var writer = new BinaryWriter(File.OpenWrite(pathString)))
                {
                    foreach (var addr in patternAddresses)
                    {
                        writer.BaseStream.Position = addr;

                        var replacementTagClassA = "";
                        var replacementTagIdA = "";

                        foreach (var c in TagClassesToReplace[0].ToCharArray())
                            replacementTagClassA = $"{replacementTagClassA}{(byte)c:X2}";

                        foreach (var c in nullTagID)
                            replacementTagIdA = $"{replacementTagIdA}{c:X2}";

                        var newArray = $"{replacementTagClassA}0000000000000000{replacementTagIdA}";
                        var newArrayBytes = HexStringArrayToHex(newArray);

                        try
                        {

                        }
                        catch
                        {
                            return false;
                        }
                        writer.Write(newArrayBytes.ToArray());

                    }
                }
            }

            Console.WriteLine("Done.");
            return true;
        }

        private static List<byte> HexStringArrayToHex(string inputSearchArrayString)
        {
            // Check for spaces in search pattern
            if (inputSearchArrayString.Contains(" "))
            {
                var a = "";
                foreach (var b in inputSearchArrayString)
                    if (b != " ".ToCharArray()[0])
                        a = $"{a}{b}";

                inputSearchArrayString = a;
            }

            // Check if search pattern is correct
            if (inputSearchArrayString.Length % 2 != 0)
            {
                Console.WriteLine($"ERROR: input pattern is not correct.");
                return new List<byte>();
            }

            var pattern = new List<byte>();
            for (int i = 0; i < inputSearchArrayString.Length; i = i + 2)
            {
                var b = $"{inputSearchArrayString[i]}{inputSearchArrayString[i + 1]}";
                byte t;
                byte.TryParse(b, NumberStyles.HexNumber, null, out t);
                pattern.Add(t);
            }

            return pattern;
        }


        private static void ChangeRasterizers(List<string> args)
        {
            var halo3rasterizers = new List<string> {
            "767473680000000000000000F0540CE27069786C0000000000000000F0550CE3",
            "767473680000000000000000F0560CE47069786C0000000000000000F0570CE5",
            "767473680000000000000000F0580CE67069786C0000000000000000F0590CE7",
            "767473680000000000000000F05A0CE87069786C0000000000000000F05B0CE9",
            "767473680000000000000000F05C0CEA7069786C0000000000000000F05D0CEB",
            "767473680000000000000000F05E0CEC7069786C0000000000000000F05F0CED",
            "767473680000000000000000F0600CEE7069786C0000000000000000F0610CEF",
            "767473680000000000000000F0620CF07069786C0000000000000000F0630CF1",
            "767473680000000000000000F0640CF27069786C0000000000000000F0650CF3",
            "767473680000000000000000F0660CF47069786C0000000000000000F0670CF5",
            "767473680000000000000000F0680CF67069786C0000000000000000F0690CF7",
            "767473680000000000000000F06A0CF87069786C0000000000000000F06B0CF9",
            "767473680000000000000000F06C0CFA7069786C0000000000000000F06D0CFB",
            "767473680000000000000000F06E0CFC7069786C0000000000000000F06F0CFD",
            "767473680000000000000000F0700CFE7069786C0000000000000000F0710CFF",
            "767473680000000000000000F0720D007069786C0000000000000000F0730D01",
            "767473680000000000000000F0740D027069786C0000000000000000F0750D03",
            "767473680000000000000000F0760D047069786C0000000000000000F0770D05",
            "767473680000000000000000F0780D067069786C0000000000000000F0790D07",
            "767473680000000000000000F07A0D087069786C0000000000000000F07B0D09",
            "767473680000000000000000F07C0D0A7069786C0000000000000000F07D0D0B",
            "767473680000000000000000F07E0D0C7069786C0000000000000000F07F0D0D",
            "767473680000000000000000F0800D0E7069786C0000000000000000F0810D0F",
            "767473680000000000000000F0820D107069786C0000000000000000F0830D11",
            "767473680000000000000000F0840D127069786C0000000000000000F0850D13",
            "767473680000000000000000F0860D147069786C0000000000000000F0870D15",
            "767473680000000000000000F0880D167069786C0000000000000000F0890D17",
            "767473680000000000000000F08A0D187069786C0000000000000000F08B0D19",
            "767473680000000000000000F08C0D1A7069786C0000000000000000F08D0D1B",
            "767473680000000000000000F08E0D1C7069786C0000000000000000F08F0D1D",
            "767473680000000000000000F0900D1E7069786C0000000000000000F0910D1F",
            "767473680000000000000000F0920D207069786C0000000000000000F0930D21",
            "767473680000000000000000F0940D227069786C0000000000000000F0950D23",
            "767473680000000000000000F0960D247069786C0000000000000000F0970D25",
            "767473680000000000000000F0980D267069786C0000000000000000F0990D27",
            "767473680000000000000000F09A0D287069786C0000000000000000F09B0D29",
            "767473680000000000000000F09C0D2A7069786C0000000000000000F09D0D2B",
            "767473680000000000000000F09E0D2C7069786C0000000000000000F09F0D2D",
            "767473680000000000000000F0A00D2E7069786C0000000000000000F0A10D2F",
            "767473680000000000000000F0A20D307069786C0000000000000000F0A30D31",
            "767473680000000000000000F0A40D327069786C0000000000000000F0A50D33",
            "767473680000000000000000F0A60D347069786C0000000000000000F0A70D35",
            "767473680000000000000000F0A80D367069786C0000000000000000F0A90D37",
            "767473680000000000000000F0AA0D387069786C0000000000000000F0AB0D39",
            "767473680000000000000000F0AC0D3A7069786C0000000000000000F0AD0D3B",
            "767473680000000000000000F0AE0D3C7069786C0000000000000000F0AF0D3D",
            "767473680000000000000000F0B00D3E7069786C0000000000000000F0B10D3F",
            "767473680000000000000000F0B20D407069786C0000000000000000F0B30D41",
            "767473680000000000000000F0B40D427069786C0000000000000000F0B50D43",
            "767473680000000000000000F0B60D447069786C0000000000000000F0B70D45",
            "767473680000000000000000F0B80D467069786C0000000000000000F0B90D47",
            "767473680000000000000000F0BA0D487069786C0000000000000000F0BB0D49",
            "767473680000000000000000F0BC0D4A7069786C0000000000000000F0BD0D4B",
            "767473680000000000000000F0BE0D4C7069786C0000000000000000F0BF0D4D",
            "767473680000000000000000F0C00D4E7069786C0000000000000000F0C10D4F",
            "767473680000000000000000F0C20D507069786C0000000000000000F0C30D51",
            "767473680000000000000000F0C40D527069786C0000000000000000F0C50D53",
            "767473680000000000000000F0C60D547069786C0000000000000000F0C70D55",
            "767473680000000000000000F0C80D567069786C0000000000000000F0C90D57",
            "767473680000000000000000F0CA0D587069786C0000000000000000F0CB0D59",
            "767473680000000000000000F0CC0D5A7069786C0000000000000000F0CD0D5B",
            "767473680000000000000000F0CE0D5C7069786C0000000000000000F0CF0D5D",
            "767473680000000000000000F0D00D5E7069786C0000000000000000F0D10D5F",
            "767473680000000000000000F0D20D607069786C0000000000000000F0D30D61",
            "767473680000000000000000F0D40D627069786C0000000000000000F0D50D63",
            "767473680000000000000000F0D60D647069786C0000000000000000F0D70D65",
            "767473680000000000000000F0D80D667069786C0000000000000000F0D90D67",
            "767473680000000000000000F0DA0D687069786C0000000000000000F0DB0D69",
            "767473680000000000000000F0DC0D6A7069786C0000000000000000F0DD0D6B",
            "767473680000000000000000F0DE0D6C7069786C0000000000000000F0DF0D6D",
            "767473680000000000000000F0E00D6E7069786C0000000000000000F0E10D6F",
            "767473680000000000000000F0E20D707069786C0000000000000000F0E30D71",
            "767473680000000000000000F0E40D727069786C0000000000000000F0E50D73",
            "767473680000000000000000F0E60D747069786C0000000000000000F0E70D75",
            "767473680000000000000000F0E80D767069786C0000000000000000F0E90D77",
            "767473680000000000000000F0EA0D787069786C0000000000000000F0EB0D79",
            "767473680000000000000000F0EC0D7A7069786C0000000000000000F0ED0D7B",
            "767473680000000000000000F0EE0D7C7069786C0000000000000000F0EF0D7D",
            "767473680000000000000000F0F00D7E7069786C0000000000000000F0F10D7F",
            "767473680000000000000000F0F20D807069786C0000000000000000F0F30D81",
            "767473680000000000000000F0BE0D4C7069786C0000000000000000F0BF0D4D",
            "767473680000000000000000F0F40D827069786C0000000000000000F0F50D83",
            "767473680000000000000000F0F60D847069786C0000000000000000F0F70D85",
            "767473680000000000000000F0F80D867069786C0000000000000000F0F90D87",
            "767473680000000000000000F0FA0D887069786C0000000000000000F0FB0D89",
            "767473680000000000000000F0FC0D8A7069786C0000000000000000F0FD0D8B",
            "767473680000000000000000F0FE0D8C7069786C0000000000000000F0FF0D8D",
            "767473680000000000000000F1000D8E7069786C0000000000000000F1010D8F",
            "767473680000000000000000F1020D907069786C0000000000000000F1030D91",
            "767473680000000000000000F1040D927069786C0000000000000000F1050D93",
            "767473680000000000000000F0C40D527069786C0000000000000000F0C50D53",
            };
            LabelStart:

            // Console.WriteLine("Input path to H3 .map file:");
            // var input = Console.ReadLine();
            // var pathString = $"D:\\Halo\\Xenia\\ISO\\Halo3Campaign\\maps\\{input}.map";
            var pathString = $"D:\\Halo\\Xenia\\ISO\\Halo3Campaign\\maps\\mainmenu.map";

            // Check file
            var path = new FileInfo(pathString);

            if (!path.Exists)
                goto LabelStart;

            var addr = 0x032ba6E8;
            // Now we should have a list of all addresses where the pattern was found

            labelRestart:
            Console.WriteLine($"Input:");
            var b = Console.ReadLine();
            var a = b.Split(" ".ToCharArray());
            var type = a[0];
            int.TryParse(a[1], out int startI);
            int.TryParse(a[2], out int endI);
            int.TryParse(a[3], out int newI);

            if (endI > 90)
                endI = 90;

            if (startI == 0 && endI == 0)
            {
                Console.WriteLine("ERROR.");
                Console.ReadLine();
                return;
            }

            if (type == "d")
            {
                using (var writer = new BinaryWriter(File.OpenWrite(pathString)))
                {
                    for (int j = startI; j < endI; j++)
                    {
                        Console.WriteLine($"Wrote DefaultShaders[{j:D2}]");
                        writer.BaseStream.Position = addr + j * 0x20;
                        writer.Write(HexStringArrayToHex(halo3rasterizers[j]).ToArray()); // 56 patchy, 90 simple
                    }
                }
            }
            else
            {
                using (var writer = new BinaryWriter(File.OpenWrite(pathString)))
                {
                    for (int j = startI; j < endI; j++)
                    {
                        Console.WriteLine($"Wrote DefaultShaders[{j:D2}]");
                        writer.BaseStream.Position = addr + j * 0x20;
                        writer.Write(HexStringArrayToHex(halo3rasterizers[newI]).ToArray()); // 56 patchy, 90 simple
                    }
                }
            }
            goto labelRestart;

            Console.WriteLine("Done.");
            Console.ReadLine();

        }


    }
}
