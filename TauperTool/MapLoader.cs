﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class MapLoader
    {
        #region Memory tool
        const int PROCESS_VM_WRITE = 0x0020;
        const int PROCESS_VM_OPERATION = 0x0008;

        [DllImport("kernel32.dll")]
        static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteProcessMemory(IntPtr hProcess, Int64 lpBaseAddress, byte[] lpBuffer, uint nSize, out int lpNumberOfBytesWritten);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadProcessMemory(IntPtr hProcess, Int64 lpBaseAddress);

        [DllImport("kernel32.dll")]
        public static extern IntPtr CloseHandle(IntPtr hProcess);

        [Flags]
        public enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            VMRead = 0x00000010,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000
        }
        #endregion

        public static void ForceLoad(List<string> args)
        {
            var addrMapRestart = 0x282BE0A58;
            var addrH3 = 0x18295E716;
            var addrMapname = addrMapRestart + 0x3C;
            var addrGameMode = addrMapRestart + 0x10;
            var addrMapnameH3 = addrH3 + 0x36;
            if (args.Count < 1)
                return;

            var mapname = args[0];
            var gameTypeString = "1";

            if (args.Count == 2)
                gameTypeString = args[1];

            Console.WriteLine($"Input mapname: (for h3 mainmenu only type 'h3')");

            bool h3 = false;
            if (H3Mapnames.Contains(mapname))
                h3 = true;

            if (h3)
            {
                addrMapRestart = addrH3;
                addrMapname = addrMapnameH3;
            }

            Process process;

            var processes = Process.GetProcessesByName("xenia");

            if (processes.Length == 0)
            {
                Console.Error.WriteLine("Unable to find any eldorado.exe processes.");
                return;
            }

            process = processes[0];

            var MapNames = new Dictionary<string, long>();

            var hProc = OpenProcess(ProcessAccessFlags.VMRead, false, (int)process.Id);

            var mapnameChars = mapname.ToCharArray();
            var mapnameBytesList = new List<byte>();
            foreach (var b in mapnameChars)
                mapnameBytesList.Add((byte)b);
            var mapnameBytes = mapnameBytesList.ToArray();

            var newMapnameBytes = new byte[16];
            for (int i = 0; i < mapnameBytes.Length; i++)
                newMapnameBytes[i] = mapnameBytes[i];

            int outs = 0;

            if (!byte.TryParse(gameTypeString, out byte gameMode))
                gameMode = 1;

            Console.WriteLine($"{addrGameMode:X16} writing gamemode flag {gameMode}");
            WriteProcessMemory(process.Handle, addrGameMode, new byte[] { 0, 0, 0, gameMode }, 4, out outs);

            Console.WriteLine($"{addrMapname:X16} writing {newMapnameBytes.Length} bytes");
            WriteProcessMemory(process.Handle, addrMapname, newMapnameBytes, (uint)newMapnameBytes.Length, out outs);

            Console.WriteLine($"{addrMapRestart:X16} writing restart flag");
            WriteProcessMemory(process.Handle, addrMapRestart, BitConverter.GetBytes(0x1), 1, out outs);

            // using (var writer = new BinaryWriter(new ProcessMemoryStream(process)))
            // {
            //     writer.BaseStream.Position = addr;
            // 
            //     writer.Write(newVal);
            // }

            return;
        }

        private static List<string> H3Mapnames = new List<string>
        {
            "h3",
            "005_intro",
            "010_jungle",
            "020_base",
            "030_outskirts",
            "040_voi",
            "050_floodvoi",
            "070_waste",
            "100_citadel",
            "110_hc",
            "120_halo",
            "130_epilogue",
            "chill",
            "construct",
            "cyberdyne",
            "deadlock",
            "guardian",
            "isolation",
            "mainmenu",
            "riverworld",
            "salvation",
            "shrine",
            "snowbound",
            "zanzibar",
        };

    }
}
