﻿using System;
using System.Windows.Forms;
using ConsoleApp;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Globalization;

namespace TauperTool
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            LabelStart:
            Console.WriteLine($"\n" +
                $"Type: \n" +
                // $"c for camera DISABLED \n" +
                $"s for memory search (VERY SLOW) \n" +
                $"w for map loading. Example: w sc140 \n" +
                $"r for batch tag replacer. Example: r sc140 null effe");

            var input = Console.ReadLine();

            if (input.Length == 0)
                goto LabelStart;

            var commands = input.Split(" ".ToCharArray()).ToList();
            var arg = commands[0];
            commands.RemoveAt(0);

            switch (arg)
            {
                case "c":
                    // var newAddr = MemoryUtils.ExecuteArraySearch2(); // var inputSearchArrayString = "000007BD0000000019001A1A"; // address around 0x31F8B0000
                    // Application.EnableVisualStyles();
                    // Application.SetCompatibleTextRenderingDefault(false);
                    // Application.Run(new Form1(newAddr));
                    break;
                case "s":
                    MemoryUtils.SearchArray(commands);
                    break;
                case "w":
                    MapLoader.ForceLoad(commands);
                    break;
                case "r":
                    if (commands.Count == 1)
                    {
                        Console.WriteLine("Details 2: Null the specified tag class. Replaces with (null).");
                        Console.WriteLine("Format  1: b <.map path> <tagnames csv path> null <tag class>");
                        Console.WriteLine("Example 1: b D:\Halo3ODST\c100.map D:\Halo3ODST\tagnames.csv l200 null prt3");
                        Console.WriteLine("");
                        Console.WriteLine("Details 2: Replace all specified tag classes with the first specified tag class. It uses hardcoded tagnames for replacement such as invalid shader or havok effect.");
                        Console.WriteLine("Format  2: b <.map path> <tagnames csv path> replace <new tag class> [tag classes to replace]");
                        Console.WriteLine("Example 2: b D:\Halo3ODST\c100.map D:\Halo3ODST\tagnames.csv l200 replace rmsh rmsh rmtr rmhg");
                        Console.WriteLine("");
                    }
                    CacheFileUtils.ReplaceTags(commands);
                    break;
            }

            goto LabelStart;
        }

    }
}