﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ConsoleApp
{
    public partial class Form1 : Form
    {
        public static Process process;
        public static long addressMain;

        public Form1(long newAddr)
        {
            addressMain = newAddr;

            var processes = Process.GetProcessesByName("xenia");

            if (processes.Length == 0)
            {
                Console.Error.WriteLine("Unable to find any eldorado.exe processes.");
                return;
            }

            process = processes[0];

            var hProc = OpenProcess(ProcessAccessFlags.VMRead, false, (int)process.Id);


            InitializeComponent();
        }

        #region Grunt Certified Memory Correction Tool
        const int PROCESS_VM_WRITE = 0x0020;
        const int PROCESS_VM_OPERATION = 0x0008;

        [DllImport("kernel32.dll")]
        static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteProcessMemory(IntPtr hProcess, Int64 lpBaseAddress, byte[] lpBuffer, uint nSize, out int lpNumberOfBytesWritten);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadProcessMemory(IntPtr hProcess, Int64 lpBaseAddress);

        [DllImport("kernel32.dll")]
        public static extern IntPtr CloseHandle(IntPtr hProcess);

        [Flags]
        public enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            VMRead = 0x00000010,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000
        }
        #endregion

        /// <summary>
        /// Struct representing a point.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public static implicit operator Point(POINT point)
            {
                return new Point(point.X, point.Y);
            }
        }

        public static Point GetCursorPosition()
        {
            POINT lpPoint;
            GetCursorPos(out lpPoint);
            //bool success = User32.GetCursorPos(out lpPoint);
            // if (!success)

            return lpPoint;
        }

        /// <summary>
        /// Retrieves the cursor's position, in screen coordinates.
        /// </summary>
        /// <see>See MSDN documentation for further information.</see>
        [DllImport("user32.dll")]
        public static extern bool GetCursorPos(out POINT lpPoint);

        // private void trackBar1_Scroll(object sender, EventArgs e)
        // {
        //     byte[] c = BitConverter.GetBytes((float)trackBar1.Value / 100);
        // 
        //     int outs = 0;
        //     WriteProcessMemory(process.Handle, addressMain, c.Reverse().ToArray(), 4, out outs);
        // }
        // 
        // private void trackBar2_Scroll(object sender, EventArgs e)
        // {
        // 
        //     byte[] c = BitConverter.GetBytes((float)trackBar2.Value / 100);
        // 
        //     int outs = 0;
        //     WriteProcessMemory(process.Handle, addressMain + 4, c.Reverse().ToArray(), 4, out outs);
        // }
        // 
        // private void trackBar3_Scroll(object sender, EventArgs e)
        // {
        // 
        //     byte[] c = BitConverter.GetBytes((float)trackBar3.Value / 100);
        // 
        //     int outs = 0;
        //     WriteProcessMemory(process.Handle, addressMain + 8, c.Reverse().ToArray(), 4, out outs);
        // }

        public static bool test = false;

        private void button2_Click(object sender, EventArgs e1)
        {
            while (true)
            {
                var a = GetCursorPosition();
            
                var hLookAngle = (float)(-a.X + 0) / 3;
                var vLookAngle = (float)(-a.Y + 500) / 6;
                hLookAngle = (float)Math.PI * hLookAngle / 180.0f;
                vLookAngle = (float)Math.PI * vLookAngle / 180.0f;
                //
                var b = (float)(Math.Cos(hLookAngle) * Math.Cos(vLookAngle));
                var c = (float)(Math.Sin(hLookAngle) * Math.Cos(vLookAngle));
                var d = (float)(Math.Sin(vLookAngle));
                var e = (float)(-Math.Cos(hLookAngle) * Math.Sin(vLookAngle));
                var f = (float)(-Math.Sin(hLookAngle) * Math.Sin(vLookAngle));
                var g = (float)(Math.Cos(vLookAngle));
                int outs = 0;
                WriteProcessMemory(process.Handle, addressMain + 0x85C, BitConverter.GetBytes(b).Reverse().ToArray(), 4, out outs);
                WriteProcessMemory(process.Handle, addressMain + 0x860, BitConverter.GetBytes(c).Reverse().ToArray(), 4, out outs);
                WriteProcessMemory(process.Handle, addressMain + 0x864, BitConverter.GetBytes(d).Reverse().ToArray(), 4, out outs);
                WriteProcessMemory(process.Handle, addressMain + 0x868, BitConverter.GetBytes(e).Reverse().ToArray(), 4, out outs);
                WriteProcessMemory(process.Handle, addressMain + 0x86C, BitConverter.GetBytes(f).Reverse().ToArray(), 4, out outs);
                WriteProcessMemory(process.Handle, addressMain + 0x870, BitConverter.GetBytes(g).Reverse().ToArray(), 4, out outs);
            
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            var a = (float)numericUpDown1.Value / 10;

            int outs = 0;
            WriteProcessMemory(process.Handle, addressMain + 0x834, BitConverter.GetBytes(a).Reverse().ToArray(), 4, out outs);
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            var a = (float)numericUpDown2.Value / 10;

            int outs = 0;
            WriteProcessMemory(process.Handle, addressMain + 0x838, BitConverter.GetBytes(a).Reverse().ToArray(), 4, out outs);
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            var a = (float)numericUpDown3.Value / 10;

            int outs = 0;
            WriteProcessMemory(process.Handle, addressMain + 0x83C, BitConverter.GetBytes(a).Reverse().ToArray(), 4, out outs);
        }

        public static float aimX = 0;
        public static float aimY = 0;

        private void numericUpDown4_ValueChanged(object sender, EventArgs e1)
        {
            aimX = (float)numericUpDown4.Value;
            aimY = (float)numericUpDown5.Value;
            var hLookAngle = (float)(-aimX);
            var vLookAngle = (float)(-aimY);
            hLookAngle = (float)Math.PI * hLookAngle / 180.0f;
            vLookAngle = (float)Math.PI * vLookAngle / 180.0f;
            var b = (float)(Math.Cos(hLookAngle) * Math.Cos(vLookAngle));
            var c = (float)(Math.Sin(hLookAngle) * Math.Cos(vLookAngle));
            var d = (float)(Math.Sin(vLookAngle));
            var e = (float)(-Math.Cos(hLookAngle) * Math.Sin(vLookAngle));
            var f = (float)(-Math.Sin(hLookAngle) * Math.Sin(vLookAngle));
            var g = (float)(Math.Cos(vLookAngle));
            int outs = 0;
            WriteProcessMemory(process.Handle, addressMain + 0x85C, BitConverter.GetBytes(b).Reverse().ToArray(), 4, out outs);
            WriteProcessMemory(process.Handle, addressMain + 0x860, BitConverter.GetBytes(c).Reverse().ToArray(), 4, out outs);
            WriteProcessMemory(process.Handle, addressMain + 0x864, BitConverter.GetBytes(d).Reverse().ToArray(), 4, out outs);
            WriteProcessMemory(process.Handle, addressMain + 0x868, BitConverter.GetBytes(e).Reverse().ToArray(), 4, out outs);
            WriteProcessMemory(process.Handle, addressMain + 0x86C, BitConverter.GetBytes(f).Reverse().ToArray(), 4, out outs);
            WriteProcessMemory(process.Handle, addressMain + 0x870, BitConverter.GetBytes(g).Reverse().ToArray(), 4, out outs);
        }

        private void numericUpDown5_ValueChanged(object sender, EventArgs e1)
        {
            aimX = (float)numericUpDown4.Value;
            aimY = (float)numericUpDown5.Value;

            var hLookAngle = (float)(-aimX);
            var vLookAngle = (float)(-aimY);
            hLookAngle = (float)Math.PI * hLookAngle / 180.0f;
            vLookAngle = (float)Math.PI * vLookAngle / 180.0f;
            var b = (float)(Math.Cos(hLookAngle) * Math.Cos(vLookAngle));
            var c = (float)(Math.Sin(hLookAngle) * Math.Cos(vLookAngle));
            var d = (float)(Math.Sin(vLookAngle));
            var e = (float)(-Math.Cos(hLookAngle) * Math.Sin(vLookAngle));
            var f = (float)(-Math.Sin(hLookAngle) * Math.Sin(vLookAngle));
            var g = (float)(Math.Cos(vLookAngle));
            int outs = 0;
            WriteProcessMemory(process.Handle, addressMain + 0x85C, BitConverter.GetBytes(b).Reverse().ToArray(), 4, out outs);
            WriteProcessMemory(process.Handle, addressMain + 0x860, BitConverter.GetBytes(c).Reverse().ToArray(), 4, out outs);
            WriteProcessMemory(process.Handle, addressMain + 0x864, BitConverter.GetBytes(d).Reverse().ToArray(), 4, out outs);
            WriteProcessMemory(process.Handle, addressMain + 0x868, BitConverter.GetBytes(e).Reverse().ToArray(), 4, out outs);
            WriteProcessMemory(process.Handle, addressMain + 0x86C, BitConverter.GetBytes(f).Reverse().ToArray(), 4, out outs);
            WriteProcessMemory(process.Handle, addressMain + 0x870, BitConverter.GetBytes(g).Reverse().ToArray(), 4, out outs);
        }
    }
}
