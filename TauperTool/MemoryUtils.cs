﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class MemoryUtils
    {
        #region Memory Tool
        const int PROCESS_VM_WRITE = 0x0020;
        const int PROCESS_VM_OPERATION = 0x0008;

        [DllImport("kernel32.dll")]
        static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool WriteProcessMemory(IntPtr hProcess, Int64 lpBaseAddress, byte[] lpBuffer, uint nSize, out int lpNumberOfBytesWritten);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadProcessMemory(IntPtr hProcess, Int64 lpBaseAddress);

        [DllImport("kernel32.dll")]
        public static extern IntPtr CloseHandle(IntPtr hProcess);

        [Flags]
        public enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            VMRead = 0x00000010,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000
        }
        #endregion

        public static bool SearchArray(List<string> args) // search readable or unaccessible memory regions
        {
            if (args.Count != 1)
                return false;

            var inputSearchArrayString = args[0];

            if (inputSearchArrayString.Contains(" "))
            {
                var a = "";
                foreach (var b in inputSearchArrayString)
                {
                    if (b != " ".ToCharArray()[0])
                    {
                        a = $"{a}{b}";
                    }
                }
                inputSearchArrayString = a;
            }

            var SearchArrayBytes1 = new List<byte>();

            for (int i = 0; i < inputSearchArrayString.Length - 2; i = i + 2)
            {
                var b = $"{inputSearchArrayString[i]}{inputSearchArrayString[i + 1]}";
                byte t;
                byte.TryParse(b, NumberStyles.HexNumber, null, out t);
                SearchArrayBytes1.Add(t);
            }
            var pattern = SearchArrayBytes1;

            Process process;

            var processes = Process.GetProcessesByName("xenia");

            if (processes.Length == 0)
            {
                Console.Error.WriteLine("Unable to find any eldorado.exe processes.");
            }

            process = processes[0];

            using (var reader = new BinaryReader(new ProcessMemoryStream(process)))
            {
                reader.BaseStream.Position = ValidAddresses[0].Address;
                LabelRestart:

                uint marker = 0;
                try
                {
                    marker = reader.ReadUInt32();
                    reader.BaseStream.Position -= 4;
                }
                catch (Exception e)
                {
                    reader.BaseStream.Position -= 4;
                    reader.BaseStream.Position = ValidAddresses[0].Address + 0x100000000;
                    goto LabelRestart;
                }

                Console.WriteLine($"Read marker at 0x{ValidAddresses[0].Address:X16} : 0x{marker:X8}");

                if (!(marker == 0x4D5A9000 || marker == 0x00905A4D))
                {
                    reader.BaseStream.Position = ValidAddresses[0].Address + 0x100000000;
                    Console.WriteLine($"Continue?");
                    Console.ReadLine();

                    goto LabelRestart;
                }

                while (true)
                {
                    // Console.WriteLine($"Region: {reader.BaseStream.Position:X16}");
                    if (reader.BaseStream.Position == 0x2832DF000)
                        ;

                    // Read a chunk of data before searching
                    var bufferSize = 0x1000 + pattern.Count;

                    LabelReread:
                    var buffer = reader.ReadBytes(bufferSize);

                    if (buffer.Length == 0)
                    {
                        reader.BaseStream.Position += 0x1000;
                        goto LabelReread;
                    }

                    // Make sure there isnt part of the pattern at the end of the buffer that wouldn't be caught. So go back the size of the pattern, before reading a new chunk for the buffer
                    reader.BaseStream.Position -= pattern.Count;

                    var bufferCounter = -1;
                    foreach (var bufferByte in buffer)
                    {
                        bufferCounter++;

                        if (bufferCounter == 0x48)
                            ;

                        if (bufferByte != pattern[0])
                            continue;

                        var foundCount = 0;
                        var patternCounter = -1;
                        foreach (var patternByte in pattern)
                        {
                            patternCounter++;

                            if (bufferCounter + patternCounter < buffer.Length)
                            {
                                var newByte = buffer[bufferCounter + patternCounter];
                                if (patternByte != newByte)
                                    goto LabelNoMatch;
                                else
                                    foundCount++;
                            }
                        }

                        if (foundCount > pattern.Count - 2)
                        {
                            var defaultAddr = reader.BaseStream.Position;
                            var tempAddr = reader.BaseStream.Position - bufferSize + pattern.Count + bufferCounter;
                            Console.WriteLine($"Found at {tempAddr:X8}");

                            reader.BaseStream.Position = tempAddr + 0xC;

                            var tagID = BitConverter.GetBytes(reader.ReadUInt32()).ToArray();

                            reader.BaseStream.Position = defaultAddr;
                            goto LabelFound;
                        }

                        LabelNoMatch:
                        ;
                    }
                }

                LabelFound:
                ;
            }

            Console.WriteLine($"Done.");

            return true;
        }

        private static List<byte> HexStringArrayToHex(string inputSearchArrayString)
        {
            // Check for spaces in search pattern
            if (inputSearchArrayString.Contains(" "))
            {
                var a = "";
                foreach (var b in inputSearchArrayString)
                    if (b != " ".ToCharArray()[0])
                        a = $"{a}{b}";

                inputSearchArrayString = a;
            }

            // Check if search pattern is correct
            if (inputSearchArrayString.Length % 2 != 0)
            {
                Console.WriteLine($"ERROR: input pattern is not correct.");
                return new List<byte>();
            }

            var pattern = new List<byte>();
            for (int i = 0; i < inputSearchArrayString.Length; i = i + 2)
            {
                var b = $"{inputSearchArrayString[i]}{inputSearchArrayString[i + 1]}";
                byte t;
                byte.TryParse(b, NumberStyles.HexNumber, null, out t);
                pattern.Add(t);
            }

            return pattern;
        }

        private class Item
        {
            public long Address;
            public string Type;
            public int Size;
        }

        private static List<Item> ValidAddresses = new List<Item>
        {
            new Item{ Address = 0x182000000,Type = "Commitable", Size = 0x12E0000 },
            new Item{ Address = 0x1832E0000,Type = "Unreadable", Size = 0x4D20000 },
            new Item{ Address = 0x188000000,Type = "Commitable", Size = 0xA0000   },
            new Item{ Address = 0x1880A0000,Type = "Unreadable", Size = 0x1360000 },
            new Item{ Address = 0x189400000,Type = "Commitable", Size = 0x70000   },
            new Item{ Address = 0x189470000,Type = "Unreadable", Size = 0x1390000 },
            new Item{ Address = 0x18A000000,Type = "Commitable", Size = 0xC0000   },
            new Item{ Address = 0x18A0C0000,Type = "Unreadable", Size = 0x1840000 },
            new Item{ Address = 0x18B900000,Type = "Commitable", Size = 0x70000   },
            new Item{ Address = 0x188970000,Type = "Unreadable", Size = 0x4690000 },
            new Item{ Address = 0x190000000,Type = "Unreadable", Size = 0x40000   },
            new Item{ Address = 0x190040000,Type = "Commitable", Size = 0x30000   },
            new Item{ Address = 0x190070000,Type = "Unreadable", Size = 0x1F90000 },
            new Item{ Address = 0x192000000,Type = "Commitable", Size = 0x12E0000 },
            new Item{ Address = 0x1932E0000,Type = "Unreadable", Size = 0x4D20000 },
            new Item{ Address = 0x198000000,Type = "Commitable", Size = 0xA0000   },
            new Item{ Address = 0x1980A0000,Type = "Unreadable", Size = 0x1360000 },
            new Item{ Address = 0x199400000,Type = "Commitable", Size = 0x70000   },
            new Item{ Address = 0x199470000,Type = "Unreadable", Size = 0x1390000 },
            new Item{ Address = 0x19A000000,Type = "Commitable", Size = 0xC0000   },
            new Item{ Address = 0x19A000000,Type = "Unreadable", Size = 0x1840000 },
            new Item{ Address = 0x198900000,Type = "Commitable", Size = 0x70000   },
            new Item{ Address = 0x198970000,Type = "Unreadable", Size = 0x4690000 },
            new Item{ Address = 0x1A0000000,Type = "Commitable", Size = 0x1000000 },
            new Item{ Address = 0x1A1000000,Type = "Unreadable", Size = 0x2F34000 },
            new Item{ Address = 0x1A3F34000,Type = "Commitable", Size = 0x1000    },
            new Item{ Address = 0x1A3F35000,Type = "Commitable", Size = 0x18D78000},
            new Item{ Address = 0x1BFCB0000,Type = "Unreadable", Size = 0x340000  },
            new Item{ Address = 0x1BFFF0000,Type = "Commitable", Size = 0xF000    },
            new Item{ Address = 0x1BFFFF000,Type = "Unreadable", Size = 0x1000    },
            new Item{ Address = 0x1C0000000,Type = "Commitable", Size = 0x1000000 },
            new Item{ Address = 0x1C1000000,Type = "Unreadable", Size = 0x2F34000 },
            new Item{ Address = 0x1C3F34000,Type = "Commitable", Size = 0x1BD7C000},
            new Item{ Address = 0x1DFCB0000,Type = "Unreadable", Size = 0x340000  },
            new Item{ Address = 0x1DFFF0000,Type = "Commitable", Size = 0xF000    },
            new Item{ Address = 0x1DFFFF000,Type = "Unreadable", Size = 0x1000    },
            new Item{ Address = 0x1E0000000,Type = "Commitable", Size = 0x1000000 },
            new Item{ Address = 0x1E1000000,Type = "Unreadable", Size = 0x2F34000 },
            new Item{ Address = 0x1E3F34000,Type = "Commitable", Size = 0x1BD7C000},
            new Item{ Address = 0x1FFCB0000,Type = "Unreadable", Size = 0x340000  },
            new Item{ Address = 0x1FFFF0000,Type = "Commitable", Size = 0xF000    },
            new Item{ Address = 0x1FFFFF000,Type = "Unreadable", Size = 0x1000    },
            new Item{ Address = 0x200000000,Type = "Commitable", Size = 0x1000000 },
            new Item{ Address = 0x201000000,Type = "Unreadable", Size = 0x2F34000 },
            new Item{ Address = 0x203F34000,Type = "Commitable", Size = 0x1000    },
        };

        private static List<Item> FindReadableMemoryBlocks(Process process)
        {
            var items = new List<Item>();

            using (var reader = new BinaryReader(new ProcessMemoryStream(process)))
            {
                reader.BaseStream.Position = 0x180000000;

                while (true)
                {
                    try
                    {
                        var a = reader.ReadByte();
                        reader.BaseStream.Position -= 1;
                        items.Add(new Item
                        {
                            Address = reader.BaseStream.Position,
                            Type = "Commitable"
                        });
                        reader.BaseStream.Position = reader.BaseStream.Position + 0x1000;
                        ;
                        if (reader.BaseStream.Position > 0x300000000)
                            return items;
                    }
                    catch
                    {
                        items.Add(new Item
                        {
                            Address = reader.BaseStream.Position,
                            Type = "Unreadable"
                        });
                        reader.BaseStream.Position = reader.BaseStream.Position + 0x1000;
                        ;
                        if (reader.BaseStream.Position > 0x300000000)
                            return items;
                    }
                }
            }
        }

        public static void ExecuteMemoryWrite()
        {
            LabelStart:

            var addr = 0x31FBEB6E8;

            Process process;

            var processes = Process.GetProcessesByName("xenia");

            if (processes.Length == 0)
            {
                Console.Error.WriteLine("Unable to find any eldorado.exe processes.");
                return;
            }

            process = processes[0];

            var MapNames = new Dictionary<string, long>();

            var hProc = OpenProcess(ProcessAccessFlags.VMRead, false, (int)process.Id);

            int outs = 0;
            WriteProcessMemory(process.Handle, addr, new byte[4], 4, out outs);

            ;

        }

    }
}
